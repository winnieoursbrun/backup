# Support Vincent LOPEZ - mail vlopez@niit.fr
#!/bin/sh

DateNow()
{
	datesynchro=$(date '+%Y-%m-%d-%H-%M')
}
Variable()
{
	#Déclaration des variables
	home_logs='/home/presta/script/logs/'
	home_save='/home/data/partage/'
	nom_logs='sync_log.txt'
	temp_logs='temp_logs.txt'
	datakey='/home/presta/.ssh/data'
}
Sync_Data()
{
	#Sauvegarde DATA
	cd $home_logs
	rsync --delete -rave  'sudo ssh -i $datakey -p 22' $home_save user@DOMAIN.TLD-BACKUP:~/backup/ >> $temp_logs
	DateNow
	echo $datesynchro" synchronisation terminé répertoire :" $home_save  >> $temp_logs
	echo " " >> $temp_logs
}
Sync_Temp()
{
	#Sync Logs Last Sync_Data
	cd $home_logs
	rsync -rave  'sudo ssh -i $datakey -p 22' $home_logs/$temp_logs user@DOMAIN.TLD-BACKUP:~/logs/
	DateNow
	echo $datesynchro" synchronisation terminé répertoire :" $home_save
	cat $temp_logs >> $nom_logs
	rm $temp_logs
}

CopyLogs()
{
	#Importation du fichier de logs dans les répertoire spécifique aux utilisateurs.
	cp $home_logs/$nom_logs $home_save/logs/
	chmod 644 $home_save/logs/$nom_logs
}
#exécution de la sauvegarde
Variable
Sync_Data
Sync_Temp
#CopyLogs